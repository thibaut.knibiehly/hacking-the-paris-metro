import pandas as pd
import numpy as np
import os

def read_dataset(file_path, sep = ';'):
    data = pd.read_csv(file_path, sep = sep)
    return data


def convert_csv_to_data(file_path, sep = ";"):
    """Convertit un fichier csv en un fichier .data (sep = " ")
    enregistre le nouveau fichier dans le même dossier avec le même nom

    :param file_path: chemin relatif du fichier .csv
    :type file_path : string or rstring
    :param sep: séparateur du fichier csv, ";" par défaut
    :type sep : string, optional
    """
    data = pd.read_csv(file_path, sep = sep).to_numpy()

    dir_file = os.path.split(file_path) # cut path in ["/dirs/", "file.ext"]
    os.chdir(dir_file[0])
    output_file_name = os.path.splitext(dir_file[1])[0] + ".data"

    with open(output_file_name, "w") as file:
        #gère close()
        pass





#pour exécuter le module en temps que script
#pratique pour les tests
if __name__ == '__main__':

    file_path_2019 = r"C:\Users\thiba\Documents\Polytechnique\INF442\shared\hacking-the-paris-metro\data\trafic-annuel-entrant-par-station-du-reseau-ferre-2019.csv"

    data = read_dataset(file_path_2019)

    assert len(data_2019) == 369
    print(data)

    convert_csv_to_data(file_path_2019)
