import pandas as pd

def make_dataframe_times(file_path):
    """colonnes trip_id, arrival_time, stop_id"""
    df = pd.read_csv(file_path, dtype={"arrival_times":str})
    df.drop(columns={"departure_time","stop_sequence","stop_headsign","shape_dist_traveled"}, inplace=True)
    return df

#pour exécuter le module en temps que script
#pratique pour les tests
if __name__ == '__main__':
    df = make_dataframe_times(r"C:\Users\Agathe\Desktop\X2018\Info\INF442\hacking-the-paris-metro\data\RATP_GTFS_RER_B_2017\stop_times.txt")
    print(df)
    print(df.dtypes)