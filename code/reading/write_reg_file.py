import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt
from sklearn import preprocessing
import code.reading.clean_stops as cs
import code.reading.clean_times as ct
import code.reading.time_th as th

def compute_distance(id1, id2, df):
    coord1=df.loc[df["stop_id"]==id1, ["stop_lon", "stop_lat"]].to_numpy()
    lon1 = coord1[0][0]
    lat1 = coord1[0][1]
    lat1rad = lat1*np.pi/180
    coord2=df.loc[df["stop_id"]==id2, ["stop_lon", "stop_lat"]].to_numpy()
    lon2 = coord2[0][0]
    lat2 = coord2[0][1]
    lat2rad = lat2*np.pi/180
    x = (lon1-lon2)*np.cos(0.5*(lat1rad+lat2rad))
    y = lat1 - lat2
    z = (x**2+y**2)**0.5
    return z*60*1.852

def make_df_reg_th(file_path_stops, file_path_times):
    """crée un dataframe avec la distance et le temps de trajet
    à partir des horaires théoriques (prédictions)
    """
    df_stops = cs.make_dataframe_stops(file_path_stops)
    df_times = th.make_dataframe_times(file_path_times)
    duree = []
    dist = []
    for i in range (1000): #donne 10000 points et s'exécute assez vite
        #donnes pour une gare i
        stop_id1 = df_times.at[i, 'stop_id']
        trip_id1 = df_times.at[i, 'trip_id']
        #on convertit la chaine de caractère en entier (nb de minutes)
        str1 = df_times.at[i, 'arrival_time']
        time1= int(str1[0])*600+int(str1[1])*60+int(str1[3])*10+int(str1[4])
        j = i+1
        trip_id2 = df_times.at[j, "trip_id"]
        while trip_id2==trip_id1:
            #donnees pour une gare j != i mais sur le même trajet
            stop_id2 = df_times.at[j, "stop_id"]
            str2 = df_times.at[j, 'arrival_time']
            time2= int(str2[0])*600+int(str2[1])*60+int(str2[3])*10+int(str2[4])
            duree.append(time2-time1)
            dist.append(compute_distance(stop_id1, stop_id2, df_stops))
            j+=1
            trip_id2 = df_times.at[j, "trip_id"]
    duree_series = pd.Series(duree, name='duree')
    distance = pd.Series(dist, name='distance')
    columns = [distance, duree_series]
    df =  pd.concat(columns, 1)
    return df

def make_df_reg(file_path_stops, file_path_times):
    """
    dataframe avec une colonne
    la distance entre les 2, 
    et le temps de trajet, 
    pour chaque couple 1 et 2 cohérents et chaque horaire dont on dispose
    """
    df_stops = cs.make_dataframe_stops(file_path_stops)
    df_times = ct.make_dataframe_times(file_path_times)
    duree = []
    dist = []
    #on parcourt toutes les stations en faisant attention au sens
    for i in range(1): 
    #idéalement, on voudrait avoir range(len(df_stops.index)) 
    # pour parcourir toutes les stations
    # mais le temps d'éxécution est très long
    # Cependant les fichiers complets ont été écrits une fois et sont ceux utilisés pour 
    # obtenir les résultats du rapport
        idA = df_stops.at[i, 'stop_id']
        nameA = df_stops.at[i, 'stop_name']
        sensA = df_stops.at[i,'sens']
        if sensA==1:
            colonne = 'next_aller'
        else:
            colonne = 'next_retour'
        #on récupère toutes les lignes correspondant à la station courante
        stationA_times = df_times.loc[df_times['stop_name']==nameA, ['time', colonne]]
        #on récupère tous les horaires ou un train part
        departA = stationA_times.loc[stationA_times['time']==stationA_times[colonne], ['time']]
        departA.reset_index(inplace=True)
        sizeA = len(departA.index)
        for j in range(len(df_stops.index)): 
            idB = df_stops.at[j, 'stop_id']
            nameB = df_stops.at[j, 'stop_name']
            #on regarde toutes les stations cohérentes avec la premiere
            if cs.are_coherent(idA, idB, df_stops):
                stationB_times = df_times.loc[df_times['stop_name']==nameB, ['time', colonne]]
                #on recupere les horaires ou un train part
                departB = stationB_times.loc[stationB_times['time']==stationB_times[colonne], ['time']]
                departB.reset_index(inplace=True)
                sizeB=len(departB.index)
                indice_max=min(sizeA, sizeB)
                for i in range(2, indice_max):
                    strA = departA.at[i, 'time']
                    intA = int(strA[-4])*600+int(strA[-3])*60+int(strA[-2])*10+int(strA[-1])
                    strB = departB.at[i, 'time']
                    intB = int(strB[-4])*600+int(strB[-3])*60+int(strB[-2])*10+int(strB[-1])
                    temps = intB-intA
                    distAB = compute_distance(idA, idB, df_stops)
                    j = i+1
                    while temps<1.82928*distAB + 0.610115-1 and j<sizeB: #critère obtenu avec la régression sur les horaires théoriques
                        strB = departB.at[j, 'time']
                        intB = int(strB[-4])*600+int(strB[-3])*60+int(strB[-2])*10+int(strB[-1])
                        temps = intB-intA
                        j+=1
                    if temps>1.82928*distAB + 0.610115-1 and temps<1.82928*distAB+ 0.610115+5: 
                        duree.append(temps)
                        dist.append(compute_distance(idA, idB, df_stops))                
    duree_series = pd.Series(duree, name='duree')
    distance = pd.Series(dist, name='distance')
    columns = [distance, duree_series]
    df =  pd.concat(columns, 1)
    return df

def write_to_csv(file_path_stops, file_path_times, path_to_save):
    df = make_df_reg(file_path_stops,file_path_times)
    df.to_csv(path_to_save, index=False, header=False)

def write_to_csv_th(file_path_stops, file_path_times, path_to_save):
    df = make_df_reg_th(file_path_stops,file_path_times)
    df.to_csv(path_to_save, index=False, header=False)

#pour exécuter le module en temps que script
#pratique pour les tests

#if __name__ == '__main__':
    #file_path_stops = r"C:\Users\Agathe\Desktop\X2018\Info\INF442\hacking-the-paris-metro\data\RATP_GTFS_RER_B_2017\stops.txt"
    #file_path_times = r"C:\Users\Agathe\Desktop\X2018\Info\INF442\hacking-the-paris-metro\data\Donnees_RER_B\0528.txt"
    #df.plot('duree', 'distance', kind='scatter')
    #plt.show()
    #write_to_csv(file_path_stops,file_path_times, r"C:\Users\Agathe\Desktop\X2018\Info\INF442\hacking-the-paris-metro\data\reg_predict.csv")