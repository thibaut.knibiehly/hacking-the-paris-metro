import pandas as pd
import numpy as np

def name(df):
    """dans le dataframe stops, remplacer les noms par ceux du df times
    (problème de tirets dans les stations avec plusieurs mots et accents)"""
    new_name = []
    for i in range(len(df.index)):
        name = df.at[i, 'stop_name']
        name=name.replace("-", " ")
        name=name.replace("é", "e")
        name=name.replace("è", "e")
        name=name.replace("â", "a")
        new_name.append(name)
    stop_name = pd.Series(new_name, name='stop_name')
    df.update(stop_name, overwrite=True)
    df.replace({"Chatelet Les Halles":"Chatelet",
    "Saint Michel Notre Dame": "Saint Michel",
    "La Courneuve Aubervilliers": "Aubervilliers",
    "Aeroport Charles de Gaulle 1": "Aeroport Ch.De Gaulle 1",
    "Aeroport Charles de Gaulle 2 TGV": "Aeroport Ch.De Gaulle 2",
    "Villeparisis": "Villeparisis Mitry",
    "Blanc Mesnil" : "Blanc-Mesnil",
    "Sevran Livry":"Sevran-Livry",
    "La Plaine Stade de France":"La Plaine-Stade de France",
    "Mitry Claye":"Mitry-Claye",
    "Vert Galant":"Vert-Galant"}, inplace=True)




def sens_aller(stop_id, df):
    """déterminer le sens d'un id :
    pour une même station, l'id le plus grand correspond à l'aller ie Saint Rémy/ CDG
    renvoie true si le stop_id est dans le sens de l'aller"""
    name = df.loc[df["stop_id"]==stop_id, "stop_name"].to_numpy()
    both_id = df.loc[df["stop_name"]==name[0], "stop_id"].to_numpy()
    if both_id[0]==stop_id:
       other_id=both_id[1]
    else :
        other_id = both_id[0]
    return stop_id>other_id

def det_branche(stop_id, df):
    """determine la branche du stop_id
    0 : branche principale
    1 : branche CDG
    2 : branche Mitry Claye
    3 : branche Saint Rémy
    4 : branche Robinson
    """
    lon_aulnay = 2.493309562581326
    lat_mitry= 48.97585585992235
    lon_sevranL = 2.534722783456677
    lon_bourg_reine = 2.3121626259597874
    lat_parc_sceaux = 48.770631071014726
    coord=df.loc[df["stop_id"]==stop_id, ["stop_lon", "stop_lat"]].to_numpy()
    lon = coord[0][0]
    lat = coord[0][1]
    if lon>lon_aulnay:
        if lon>=lon_sevranL and lat<=lat_mitry:
            return 2
        return 1
    if lat<=lat_parc_sceaux:
        return 3
    if lon<lon_bourg_reine:
        return 4
    return 0

def are_coherent(stop_id1, stop_id2, df):
    b1 = det_branche(stop_id1,df)
    b2 = det_branche(stop_id2, df)
    #On teste si les 2 arrets sont bien dans le meme sens et si ils sont sur 2 branches cohérentes
    #1 est seulement cohérent avec 1 et 3, et 2 avec 2 et 4
    if stop_id1!=stop_id2 and sens_aller(stop_id1,df)==sens_aller(stop_id2, df) and (b1==b2 or b1==b2+2 or b2==b1+2 or b1==0 or b2==0):
        return True
    return False

def make_dataframe_stops(file_path):
    """dataframe avec les colonnes
    stop_id
    stop_name
    sens
    branche
    stop_lat
    stop_lon
    """
    df = pd.read_csv(file_path)
    df.drop(columns={"stop_code","stop_desc","location_type","parent_station"}, inplace=True)
    #on ajoute la colonne de sens et celle de la branche
    sens = np.zeros(len(df.index))
    branche = np.zeros(len(df.index))
    for i in range(len(df.index)):
        stop_id = df.at[i, 'stop_id']
        sens[i]=sens_aller(stop_id, df)   #1=aller, 0=retour
        branche[i]=det_branche(stop_id, df)
    df.insert(2, "sens", sens)
    df.insert(3, "branche", branche)
    name(df)
    return df

def make_file_right_names(file_path, path_to_save):
    """3 colonnes : 
    stop_name avec le même nom que les fichiers d'horaires réels
    stop_lat
    stop_lon"""
    df = pd.read_csv(file_path)
    df.drop(columns={"stop_id","stop_code","stop_desc","location_type","parent_station"}, inplace=True)
    name(df)
    print(df)
    df.to_csv(path_to_save, index=False, header=False)

#pour exécuter le module en temps que script
#pratique pour les tests
if __name__ == '__main__':
    make_file_right_names(r"C:\Users\Agathe\Desktop\X2018\Info\INF442\hacking-the-paris-metro\data\RATP_GTFS_RER_B_2017\stops.txt", r"C:\Users\Agathe\Desktop\X2018\Info\INF442\hacking-the-paris-metro\data\coords.csv")
    