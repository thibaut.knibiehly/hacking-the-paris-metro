import numpy as np
import os as os
import pandas as pd
os.chdir('C:\\Users\\thiba\\Documents\\Polytechnique\\INF442\\shared\\hacking-the-paris-metro\\code\\reading')
import matplotlib.pyplot as plt


def compute_distance(stations):
    """retourne la liste des distances entre deux gares consécutives"""
    res = [0]
    for i in range(1, len(stations)):
        lon1 = stations.longitude.get(i)
        lat1 = stations.latitude.get(i)
        lat1rad = lat1*np.pi/180

        lon2 = stations.longitude.get(i-1)
        lat2 = stations.latitude.get(i-1)
        lat2rad = lat2*np.pi/180
        x = (lon1-lon2)*np.cos(0.5*(lat1rad+lat2rad))
        y = lat1 - lat2
        z = (x**2+y**2)**0.5
        res.append(z*60*1.852)
    return res

def timea_minus_timeb(a, b):
    if a < b:
        return - timea_minus_timeb(b, a)
    timea = int(str(a)[-4:-2])*60 + int(str(a)[-2:])
    timeb = int(str(b)[-4:-2])*60 + int(str(b)[-2:])
    return (timea - timeb) % (24 * 60)

def read_file(nom, len_train_line,names):
    '''ouvre un fichier csv et renvoie la liste de ses valeurs'''
    aller, retour = [], []
    file = open(nom, 'r')
    EOF = False
    while not EOF:
        #pour chaque pas de temps
        stations_aller, stations_retour = [], []
        for station in range(len_train_line):
            #pour chaque gare
            ligne = file.readline()
            if not ligne:
                EOF = True
                break
            line = ligne.split("\t")
            assert(line[0] == names[station])
            nb_aller, nb_retour = int(line[4]), int(line[6])

            stations_aller.append([int(string) for string in line[7:7+nb_aller]])

            stations_retour.append([int(string) for string in line[7+nb_aller:7+nb_aller+nb_retour]])

        assert len(stations_aller) == len(stations_retour)
        assert len(stations_aller) == 0 or len(stations_aller) == len_train_line

        if not EOF:
            aller.append(stations_aller)
            retour.append(stations_retour)


    file.close()
    return aller, retour

def detect_lates_aller(data, stations):
    '''
    En pratique le paramètre sera un des retour de read_file
    '''
    nb_t = len(data)
    nb_station = len(data[0])

    lates = [[] for l in range(nb_station)]

    for t in range(0, nb_t-1): # on a besoin d'avoir accès au pas de temps  suivant
        for station in range(1, nb_station): # on a besoin d'avoir accès à l astation d'avant
            if len(data[t][station]) == 0:
                continue
            horaire_prochain_train = data[t][station][0]
            # si le train est parti à l'instant suivant
            if len(data[t+1][station]) == 0:
                continue
            if timea_minus_timeb(data[t+1][station][0], horaire_prochain_train) > 3: # metaparamètre à ajuster
                #on regarde quand est-ce qu'il est parti de la gare précédente
                t_depart = t-1
                while (t_depart > 0):
                    if len(data[t_depart][station-1]) == 0 or len(data[t_depart-1][station-1])==0:
                        break
                    if timea_minus_timeb(data[t_depart][station-1][0], data[t_depart-1][station-1][0]) > 3:
                        #alors le train est parti à t_depart-1 de la station précédente
                        #on teste s'il a pris du retard sur ce tronçon
                        if len(data[t_depart][station]) == 0:
                            break
                        late = timea_minus_timeb(horaire_prochain_train, data[t_depart][station][0])
                        if late > 2 and late < 10:
                            lates[station].append([late, t])
                        break
                    else:
                        t_depart -= 1
    return lates


def detect_lates_retour(data, stations):
    '''
    En pratique le paramètre sera un des retour de read_file
    '''
    nb_t = len(data)
    nb_station = len(data[0])

    lates = [[] for l in range(nb_station)]

    for t in range(0, nb_t-1): # on a besoin d'avoir accès au pas de temps  suivant
        for station in range(nb_station-1):
            if len(data[t][station]) == 0:
                continue
            horaire_prochain_train = data[t][station][0]
            # si le train est parti à l'instant suivant
            if len(data[t+1][station]) == 0:
                continue
            if timea_minus_timeb(data[t+1][station][0], horaire_prochain_train) > 3: # metaparamètre à ajuster
                #on regarde quand est-ce qu'il est parti de la gare précédente
                t_depart = t-1
                while (t_depart > 0):
                    if len(data[t_depart][station+1]) == 0 or len(data[t_depart-1][station+1])==0:
                        break
                    if timea_minus_timeb(data[t_depart][station+1][0], data[t_depart-1][station+1][0]) > 3:
                        #alors le train est parti à t_depart-1
                        #on teste s'il a pris du retard sur ce tronçon
                        if len(data[t_depart][station]) == 0:
                            break
                        late = timea_minus_timeb(horaire_prochain_train, data[t_depart][station][0])
                        if late > 2 and late < 10:
                            lates[station].append([late, t])
                        break
                    else:
                        t_depart -= 1
            else:
                pass

    return lates

def build_lates_df(lates, names):
    df = pd.DataFrame({'gare' : names, 'lates' : lates})
    return df

def get_station(nom, len_train_line):
    names = []
    file = open(nom, 'r')
    EOF = False
    for i in range(len_train_line):
        ligne = file.readline()
        if not ligne:
            EOF = True
            break
        line = ligne.split("\t")
        names.append(line[0])
    assert not EOF
    file.close()
    return names



def read_station_coords(filename):
    df = pd.read_csv(filename, sep = ',')
    return df


def merge_df(dfa, dfb):
    return pd.merge(dfa, dfb, how='inner', on='gare', left_on=None, right_on=None, left_index=False, right_index=False, sort=False, suffixes=('_aller', '_retour'), copy=True, indicator=False, validate=None)

def write_df_naif(df):

    output_file_name = "..\\..\\data\\temp\\lates_naive" + ".data"
    with open(output_file_name, "w") as file:
        #gère close()
        file.write("nb_lates\n")
        for i in range(len(df)):
            file.write(str(len(df.lates_aller.get(i))+ len(df.lates_retour.get(i)))+"\n")


def write_df(df, dilat_time = 1):

    output_file_name = "..\\..\\data\\temp\\lates" + ".data"
    with open(output_file_name, "w") as file:
        #gère close()
        file.write("num_station\ttemps\n")

        for i in range(len(df)):
            for j in range(len(df.lates_aller.get(i))):
                file.write(str(i * dilat_time) + "\t" + str(df.lates_aller.get(i)[j][1]) + "\n")
            for j in range(len(df.lates_retour.get(i))):
                file.write(str(i * dilat_time) + "\t" + str(df.lates_retour.get(i)[j][1]) + "\n")




if __name__ == '__main__':
    nom = '..\\..\\data\\Donnees_RER_B\\0527_clean'
    station_filename = '..\\..\\data\\coords_clean.csv'
    len_train_line = 47 # nombre de stations

    beta = (0.61, 1.82)

    names = get_station(nom, len_train_line)
    names_df = pd.DataFrame({'gare' : names})
    allers, retours = read_file(nom, len_train_line, names)

    stations = read_station_coords(station_filename)
    #pour trier les gares dans l'ordre de la ligne
    stations = pd.merge(names_df, stations, how='inner', on='gare')

    distance = compute_distance(stations)
    stations['distance'] = distance
    stations['temps'] = [beta[0]+stations.distance.get(k)*beta[1] for k in range(len(stations))]

    lates_aller = detect_lates_aller(retours, stations)
    lates_retour = detect_lates_retour(allers, stations)

    lates_aller = lates_aller[1:28]
    lates_retour = lates_retour[0:27]
    names = names[0:27]

    lates_aller_df = build_lates_df(lates_aller, names)
    lates_retour_df = build_lates_df(lates_retour, names)

    lates_df = merge_df(lates_aller_df, lates_retour_df)

    df = merge_df(lates_df, stations)
    write_df_naif(df)

    dilat_time = 15 #metaparamtre
    write_df(df, dilat_time)


    #print(lates_aller_df)

    #plt.plot(range(len_train_line), lates)
    #plt.show()
