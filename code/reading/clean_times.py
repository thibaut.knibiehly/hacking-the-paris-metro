import pandas as pd

def tirets(df):
    """enlever tous les tirets dans les noms"""
    new_name = []
    for i in range(len(df.index)):
        name = df.at[i, 0]
        name=name.replace("-", " ")
        new_name.append(name)
    stop_name = pd.Series(new_name, name='stop_name')
    df.update(stop_name, overwrite=True)

def make_dataframe_times(file_path):
    """cree un dataframe avec les colonnes
    nom
    heure
    prochain aller
    prochain retour
    """
    df_init = pd.read_table(file_path, sep='\t',header=None, usecols=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19],
    dtype={0:str,1:str,2:int,3:str,4:int,5:str,6:int,7:str,8:str,9:str,10:str,11:str,12:str,13:str,14:str,15:str,16:str,17:str,18:str,19:str})
    stop_name = df_init[0].rename('stop_name')
    time=df_init[1].rename('time')
    next_aller=df_init[7].rename('next_aller')
    next_retour_list = []
    for i in range(len(df_init.index)):
        indice_retour = 7 + df_init.at[i,4]
        next_retour_list.append(df_init.at[i,indice_retour])
    next_retour = pd.Series(next_retour_list, name='next_retour')
    columns = [stop_name, time, next_aller, next_retour]
    df =  pd.concat(columns, 1)
    return df

#pour exécuter le module en temps que script
#pratique pour les tests
#if __name__ == '__main__':
    # tests and example
