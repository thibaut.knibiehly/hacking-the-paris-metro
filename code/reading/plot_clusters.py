import os as os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import datetime

#os.chdir('C:\\Users\\thiba\\Documents\\Polytechnique\\INF442\\shared\\hacking-the-paris-metro\\code\\reading')

viridis = cm.get_cmap('tab20', 20)

def plot_2D(data, dilat_time):
    for i in range(len(data)):
        plt.scatter(data.num_station.get(i)//dilat_time, data.temps.get(i), color = viridis.colors[data.label.get(i)])
    plt.xlabel("Numéro de la gare entre St Rémy les Chévreuses et Châtelet")
    #plt.legend()
    plt.ylabel("Heure (en nombre de minutes après 16h43)")
    plt.title("Représentation des retards dans l'espace et dans le temps")
    plt.show()

def plot_naive(data_naive, nb_station):
    plt.plot(range(nb_station), data_naive.nb_lates, label = None)
    plt.xlabel("Numéro de la gare entre St Rémy les Chévreuses et Châtelet")
    plt.legend()
    plt.ylabel("Nombre de retards")
    plt.title("Nombre de retards par gare de départ")
    plt.show()


if __name__ == '__main__':

    nb_station = 27
    dilat_time = 15
    file_path = '..\\..\\outputs\\temp\\lates.out'
    file_path_naive = '..\\..\\data\\temp\\lates_naive.data'
    data = pd.read_csv(file_path, sep = '\t')
    data_naive = pd.read_csv(file_path_naive, sep = '\t')

    plot_2D(data, dilat_time)
    #plot_naive(data_naive, nb_station)
