#include <iostream>
#include <cassert>
#include <cfloat>	// for DBL_MAX
#include <fstream>
#include <cstdlib>	// for rand, srand
#include <ctime>	// for rand seed
#include <string>

#include "cloud.hpp"
#include "MyArea.hpp"

using std::ifstream;
using std::cout; using std::endl;

int main(int argc, char **argv)
{
  int nmax_temp = 1;

  if(argc < 6 || argc > 7)
  {
    std::cerr << "Usage: " << argv[0] << " csv nb_clusters x_column y_column dimension [nmax]" << std::endl;
    std::exit(1);
  }
  std::string csv_filename = argv[1];
  const int k = std::stoi(argv[2]);
  int x_column = std::stoi(argv[3]);
  int y_column = std::stoi(argv[4]);
  const int d = std::stoi(argv[5]);


  if(argc >= 7)
    nmax_temp = std::stoi(argv[6]);

  const int nmax = nmax_temp;

  srand(time(NULL));

  cloud c(d, nmax, k);

  // open data file
  ifstream is(csv_filename);
  assert(is.is_open());

  // read header line
  std::string header_line;
	std::getline(is, header_line);

  // point to read into
  point p;

  // labels to cycle through
  int label = 0;

  // while not at end of file
  while(is.peek() != EOF)
    {
      // read new points
      for(int m = 0; m < d; m++)
	{
	  is >> p.coords[m];
	}

      c.add_point(p, label);

      label = (label + 1) % k;

      // read ground-truth labels
      // unused in normal operation
      // std::string next_name;
      // is >> next_name;

      // consume \n
      is.get();
    }

  // execute k-means algorithm
  cout << "Intracluster variance before k-means: "
       << c.intracluster_variance()
       << endl;

  c.kmeans();

  cout << "Intracluster variance after k-means: "
       << c.intracluster_variance()
       << endl;

  std::cout << c.get_n() <<std::endl;
  // launch graphical interface
  int gtk_argc = 1;
  Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(gtk_argc, argv, "inf442.td3");

  Gtk::Window win;
  win.set_title("Hacking the Paris Metro");
  win.set_default_size(600, 600);

  MyArea area(&c, x_column, y_column);
  win.add(area);
  area.show();

  // ecriture
  std::string output_fn = std::string(csv_filename);
  output_fn.erase(output_fn.begin()+6, output_fn.begin()+10);
  output_fn.insert(6, "outputs");
  output_fn.erase(output_fn.end()-4, output_fn.end());
  output_fn.insert(output_fn.length(), "out");
  std::cout << output_fn << std::endl;
  c.write_output(output_fn, header_line);

  return app->run(win);
}
