#include "LinearRegression.hpp"
#include "Dataset.hpp"
#include "Regression.hpp"
#include<iostream>
#include<cassert>


LinearRegression::LinearRegression( Dataset* dataset, int col_regr ) 
: Regression(dataset, col_regr) {
	SetCoefficients();
}

LinearRegression::~LinearRegression() {
	 delete m_beta;
}

void LinearRegression::SetCoefficients() {
	int d = m_dataset->GetDim();
	int n = m_dataset->GetNbrSamples();
	m_beta = new Eigen::VectorXd(d);
	Eigen::VectorXd y(n);
	for(int i =0; i<n; i++) y(i) = m_dataset->GetInstance(i)[m_col_regr];
	Eigen::MatrixXd X(n,d);
	for(int i = 0; i<n; i++){
		X(i,0)=1;
        for(int j=1; j<d; j++){
            if(j<=m_col_regr) X(i,j) = m_dataset->GetInstance(i)[j-1];
            if(j>m_col_regr) X(i,j) = m_dataset->GetInstance(i)[j];
        }
    }
	Eigen::VectorXd beta(d);
	beta = ((X.transpose()*X).inverse()*X.transpose())*y;
	*m_beta = beta;
}

void LinearRegression::ShowCoefficients() {
	if (!m_beta) {
		std::cout<<"Coefficients have not been allocated."<<std::endl;
		return;
	}
	
	if (m_beta->size() != m_dataset->GetDim()) {  // ( beta_0 beta_1 ... beta_{d} )
		std::cout<< "Warning, unexpected size of coefficients vector: " << m_beta->size() << std::endl;
	}
	
	std::cout<< "beta = (";
	for (int i=0; i<m_beta->size(); i++) {
		std::cout<< " " << (*m_beta)[i];
	}
	std::cout << " )"<<std::endl;
}

void LinearRegression::SumsOfSquares( Dataset* dataset, double& ess, double& rss, double& tss ){
	assert(dataset->GetDim()==m_dataset->GetDim());
	int d = dataset->GetDim();
	int n = dataset->GetNbrSamples();
	ess = 0;
	tss = 0;
	rss = 0;
	Eigen::MatrixXd X(n,d-1);
	Eigen::VectorXd Y(n);
	Eigen::VectorXd est(n);
	for(int i = 0; i<n; i++){
		Y(i) = dataset->GetInstance(i)[m_col_regr];
        for(int j=0; j<d-1; j++){
            if(j<m_col_regr) X(i,j) = dataset->GetInstance(i)[j];
            if(j>m_col_regr) X(i,j) = dataset->GetInstance(i)[j+1];
		}
		est(i)=Estimate(X.row(i));
	}
	double moy = Y.mean();
	for(int i =0; i<n; i++){
		tss+=(Y(i)-moy)*(Y(i)-moy);
		ess+=(est(i)-moy)*(est(i)-moy);
		rss+=(est(i)-Y(i))*(est(i)-Y(i));
	}
}

double LinearRegression::Estimate( const Eigen::VectorXd & x ) {
	int d = m_dataset->GetDim();
	Eigen::VectorXd coef(d-1);
	for(int i=0; i<d-1; i++) coef(i) = (*m_beta)(i+1);
	double a= (*m_beta)(0);
	Eigen::MatrixXd b(1,1); 
	b = x.transpose()*coef; 
	return a+b(0,0);
}
