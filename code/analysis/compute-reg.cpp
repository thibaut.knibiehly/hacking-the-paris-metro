#include "LinearRegression.hpp"
#include "Dataset.hpp"
#include <iostream>
#include <cstdlib>
#include <stdio.h>

/** @file
 * Test suite for the LinearRegression class.
 * This executable will put the two provided CSV files (train and test) in objects of class Dataset, perform linear regression on the provided column, and print the resulting test set MSE.
*/

int main(int argc, const char * argv[]){
	if (argc < 3) {
        std::cout << "Usage: " << argv[0] << " <train_file> <test_file> [ <column_for_regression> ]" << std::endl;
        return 1;
    }

	std::cout<< "Reading training dataset "<<argv[1]<<"..."<<std::endl;

	Dataset train_dataset(argv[1]);
	Dataset regr_dataset(argv[2]);
    
    int col_regr;
    if (argc == 4) {
    	col_regr = atoi(argv[3]);
    } else {
		col_regr = train_dataset.GetDim()-1;
		std::cout<< "No column specified for regression, assuming last column of dataset ("<< col_regr <<")."<<std::endl;    
    }

	train_dataset.Show(false);  // only dimensions and samples

	assert(train_dataset.GetDim() == regr_dataset.GetDim()); 	// otherwise doesn't make sense
	
	std::cout<< "Computing linear regression coefficients (regression over column "<< col_regr << ")..."<<std::endl;
	LinearRegression tester(&train_dataset, col_regr);

	tester.ShowCoefficients();

	double ess, rss, tss;
	tester.SumsOfSquares(&train_dataset, ess, rss, tss);
	std::cout << "Sums of Squares wrt training set:"<< std::endl << " ESS=" << ess << std::endl <<" RSS="<< rss << std::endl << " TSS="<<tss <<std::endl<<" R2="<<ess/tss << std::endl <<" MSE="<< rss/train_dataset.GetNbrSamples() << std::endl;
	tester.SumsOfSquares(&regr_dataset, ess, rss, tss);
	std::cout << "Sums of Squares wrt regression set:"<< std::endl << " ESS=" << ess << std::endl <<" RSS="<< rss << std::endl << " TSS="<<tss <<std::endl<<" MSE="<< rss/regr_dataset.GetNbrSamples() <<std::endl;
		
	return 0;
}
