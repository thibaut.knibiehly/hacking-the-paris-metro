import pandas as pd
import numpy as np
#import code.reading.read_and_clean as rd
#import code.reading.read_lates as lt
#import code.reading.write_reg_file as wrf
import matplotlib.pyplot as plt

os.chdir('C:\\Users\\thiba\\Documents\\Polytechnique\\INF442\\shared\\hacking-the-paris-metro')
'''
file_path_2019 = "data/trafic-annuel-entrant-par-station-du-reseau-ferre-2019.csv"
data_2019 = rd.read_dataset(file_path_2019)
print(len(data_2019))
'''
# plot stations
file_path_stops = "data/RATP_GTFS_RER_B_2017/stops.txt"
data_stops = pd.read_csv(file_path_stops, sep = ",")
#data_stops = data_stops.get(["stop_name", "stop_lat", "stop_lon"])

data_stops.plot(x = "stop_lon", y = "stop_lat", kind = "scatter", marker = '.', linewidths = 0.1)
fig = plt.gcf()
fig.set_size_inches(20, 10)
axes = plt.gca()
axes.set_xlim([1.9,2.9])
axes.set_ylim([48.6,49.1])
fig.savefig('./pictures/stops.png', dpi=300)

plt.show()


"""Pour créer les fichiers csv nécessaires pour la régression
Attention, le temps d'exécution peut être un peu long"""
"""
file_path_stops = "data/RATP_GTFS_RER_B_2017/stops.txt"

#Fichier avec les horaires théoriques
file_path_th = "data/RATP_GTFS_RER_B_2017/stop_times.txt"
file_path_to_write = "data/Fichiers_reg/reg_train_th.csv"
wrf.write_to_csv_th(file_path_stops,file_path_th, file_path_to_write)
print("Fichier à partir des horaires théoriques : done")

#Ficher avec les horaires en temps réel pour le 27/05
file_path_times = "data/Donnees_RER_B/0527"
file_path_to_write = "data/Fichiers_reg/reg_train_0527_reduced.csv"
wrf.write_to_csv(file_path_stops,file_path_times, file_path_to_write)
print("Fichier à partir des horaires réels du 27/05 : done")

#Ficher avec les horaires en temps réel pour le 28/05
file_path_times = "data/Donnees_RER_B/0528"
file_path_to_write = "data/Fichiers_reg/reg_test_0528_reduced.csv"
wrf.write_to_csv(file_path_stops,file_path_times, file_path_to_write)
print("Fichier à partir des horaires réels du 28/05 : done")
"""
