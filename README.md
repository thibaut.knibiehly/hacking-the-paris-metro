# Hacking the Paris metro

Big data analysis of real-time RATP data
INF442 project
Agathe et Thibaut

This worked is published under the ODbL license.

https://data.ratp.fr/explore/

A report is written here :

https://www.overleaf.com/read/mpzkmyrpjjfx

# Régression

## Création des fichiers nécessaires à la régression

Exécuter main.py à la racine du projet permet des créér les trois fichiers csv nécessaires à la régression.
Remarque : Ces fichiers ont tous déjà été créés et enregistrés et cette étape n'est donc pas indispensable. De plus, par souci de rapidité d'exécution, les fichiers créés ici (notés _reduced) seront moins gros que ceux déjà enregistrés.

## Régression linéaire

Dans le répertoire code/analysis :
 - Dans le fichier Makefile, mettre à jour le chemin de la bibliothèque Eigen
 - make compute-reg
 - ./compute-reg <train-file> <test-file> 1

Pour train-file, on peut utiliser data/Fichiers-reg/reg_train_th.csv ou data/Fichiers_reg/reg_train_0527.csv. Pour test-file, on peut utiliser data/Fichiers_reg/reg_test_0528.csv.
Pour utiliser les fichiers créés à l'étape précédente, ajouter _reduced au nom de fichier.

# Clustering
Pour construire le jeu de données lisibles par l'implémentation C++ de kmean, il faut exécuter code/reading/read_lates.py en console avec python3. Cela nécessite l'installation des librairies pandas et numpy.
Deux fichiers sont ainsi créés ou mis à jour :
- data/temp/lates_naive.data
- data/temp/lates.data

Le fichier lates_naive.data permet d'afficher le nombre de retards par gare alors que le second sera le support du Clustering

Pour lancer cet algorithme, il faut compiler cloud-run (nécessite une mise à jour du chemin de la librairie EIGEN comme pour la régression), puis effectuer dans le répertoire code/analysis la commande :

./cloud-run "../../data/temp/lates.data" 8 0 1 2 45000

Après le chemin d'accès les arguments sont : le nombre de cluster (<=20), la colonne représentant l'axe des abscisses pour l'affichage, celle représentant l'axe des ordonnées (comme au TD3), la dimension et finalement le nombre maximal de point du nuage

Les paramètres par défaut sont réglés dans run.sh et le clustering peut aussi être exécuté en lançant ./run.sh depuis la racine du projet.

Le clustering crée deux nouveaux fichiers dans outputs/temp :
- lates_naive.out
- lates.out
dont une copie csv a été placé dans le projet pour les paramètres par défaut

Pour un nombre de cluster supérieur à 4, l'affichage ne se fait pas avec assez de couleurs en C++, l'affichage peut se faire avec plot_cluster.py à lancer avec python en console depuis le répertoire code/reading.

Remarque : l'affichage nécessite la génération au préalable des fichiers .out. Il faut sinon changer la ligne 35 de plot_clusters.py en changeant le .out en .csv
