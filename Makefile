SUBDIR = code/analysis

analysis:
	cd $(SUBDIR) && $(MAKE)

clean:
	$(MAKE) -C $(SUBDIR) clean
